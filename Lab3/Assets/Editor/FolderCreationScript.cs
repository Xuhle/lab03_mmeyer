﻿using UnityEditor;
using UnityEngine;

public class FolderCreationScript : MonoBehaviour
{

    [MenuItem("Tool Creation/Create folder")]

    public static void CreateFolder()
    {
        //Create a folder named Materials in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Materials");
        //Create a file named "folderStructure.txt" in the Assets/Materials folder
        System.IO.File.WriteAllText(Application.dataPath + "/Materials/folderStructure.txt", "This folder is for storing materials!");

        //Create a folder named Textures in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Textures");
        //Create a file named "folderStructure.txt" in the Assets/Textures folder
        System.IO.File.WriteAllText(Application.dataPath + "/Textures/folderStructure.txt", "This folder is for storing textures!");

        //Create a folder named Prefabs in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Prefabs");
        //Create a file named "folderStructure.txt" in the Assets/Prefabs folder
        System.IO.File.WriteAllText(Application.dataPath + "/Prefabs/folderStructure.txt", "This folder is for storing Prefabs!");

        //Create a folder named Scripts in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Scripts");
        //Create a file named "folderStructure.txt" in the Assets/Scripts folder
        System.IO.File.WriteAllText(Application.dataPath + "/Scripts/folderStructure.txt", "This folder is for storing Scripts!");

        //Create a folder named Scenes in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Scenes");
        //Create a file named "folderStructure.txt" in the Assets/Scenes folder
        System.IO.File.WriteAllText(Application.dataPath + "/Scenes/folderStructure.txt", "This folder is for storing Scenes!");

        //Create a folder named Animations in the Assets folder
        AssetDatabase.CreateFolder("Assets", "Animations");
        //Create a file named "folderStructure.txt" in the Assets/Animations folder
        System.IO.File.WriteAllText(Application.dataPath + "/Animations/folderStructure.txt", "This folder is for storing raw Animations!");

        //Create a folder named AnimationControllers in the Animations folder
        AssetDatabase.CreateFolder("Assets/Animations", "AnimationControllers");
        //Create a file named "folderStructure.txt" in the Assets/Animations/AnimationControllers folder
        System.IO.File.WriteAllText(Application.dataPath + "/Animations/AnimationControllers/folderStructure.txt", "This folder is for storing Animation Controllers!");

        //Refresh the project structure to commit all changes
        AssetDatabase.Refresh();
    }
}