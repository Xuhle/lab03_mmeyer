﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class SpriteCreationScript : EditorWindow
{
    public static object selectedObject;

    int numAnimations;
    string controllerName;
    string[] animationNames = new string[100];
    float[] clipFrameRate = new float[100];
    float[] clipTimeBetween = new float[100];
    int[] startFrames = new int[100];
    int[] endFrames = new int[100];
    bool[] pingPong = new bool[100];
    bool[] loop = new bool[100];

    [MenuItem("Tool Creation/Create 2D animation")]

    static void Init()
    {
        selectedObject = Selection.activeGameObject;

        if(selectedObject == null)
        {
            return;
        }

        SpriteCreationScript window = (SpriteCreationScript)EditorWindow.GetWindow(typeof(SpriteCreationScript));
        window.Show();
    }
}
